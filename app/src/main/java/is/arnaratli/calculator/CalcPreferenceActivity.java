package is.arnaratli.calculator;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by arnar on 8/30/15.
 */
public class CalcPreferenceActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}
