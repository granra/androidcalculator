package is.arnaratli.calculator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    private TextView display_m;
    private Vibrator vibrator_m;
    private Boolean use_vibrator_m = false;
    SharedPreferences sp_m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/digital7.ttf");
        display_m = (TextView) findViewById(R.id.display);
        display_m.setTypeface(typeface);

        vibrator_m = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        sp_m = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        use_vibrator_m = sp_m.getBoolean("vibrate", false);
        switch(sp_m.getString("color", "G")) {
            case "R":
                display_m.setTextColor(Color.RED);
                break;
            case "G":
                display_m.setTextColor(Color.GREEN);
                break;
            case "B":
                display_m.setTextColor(Color.BLUE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, CalcPreferenceActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonPressed(View view) {
        Button buttonView = (Button) view;
        String text = null;

        switch(view.getId()) {
            case R.id.buttonEq:
                display_m.setText(evaluate(display_m.getText().toString()));
                break;
            case R.id.buttonPlus:
            case R.id.buttonMinus:
                text = display_m.getText().toString();
                if(!(text.endsWith("+") || text.endsWith("-"))) {
                    display_m.append(buttonView.getText());
                }
                break;
            case R.id.buttonBack:
                text = display_m.getText().toString();
                if(text.length() > 0) {
                    display_m.setText(text.substring(0, text.length()-1));
                }
                break;
            case R.id.buttonC:
                display_m.setText("");
                break;
            default:
                display_m.append(buttonView.getText());
        }

        if(use_vibrator_m && vibrator_m.hasVibrator()) {
            vibrator_m.vibrate(100);
        }
    }

    private String evaluate(String sequence) {
        StringTokenizer st = new StringTokenizer(sequence, "[+\\-]", true);
        String token = "";
        BigInteger result = BigInteger.ZERO;
        // Parse first number
        if (st.hasMoreTokens()) result = new BigInteger(st.nextToken());
        while (st.hasMoreElements()) {
            token = st.nextToken();

            switch (token) {
                case "+":
                    result = result.add(new BigInteger(st.nextToken()));
                    break;
                case "-":
                    result = result.subtract(new BigInteger(st.nextToken()));
                    break;
            }
        }

        return result.toString();
    }
}
